<?php

namespace Bitkorn\Cashbook\Entity;

use Bitkorn\Trinket\Entity\AbstractEntity;

class CostEntity extends AbstractEntity
{
    public array $mapping = [
        'cost_uuid'   => 'cost_uuid',
        'client_uuid' => 'client_uuid',
        'cost_type'   => 'cost_type',
        'cost_label'  => 'cost_label',
        'cost_net'    => 'cost_net',
        'cost_taxp'   => 'cost_taxp',
        'cost_gross'  => 'cost_gross',
        'cost_date'   => 'cost_date',
        'cost_docno'  => 'cost_docno',
    ];

    protected $primaryKey = 'cost_uuid';

    public function getCostUuid(): string
    {
        if (!isset($this->storage['cost_uuid'])) {
            return '';
        }
        return $this->storage['cost_uuid'];
    }

    public function setCostUuid(string $costUuid): void
    {
        $this->storage['cost_uuid'] = $costUuid;
    }

    public function getClientUuid(): string
    {
        if (!isset($this->storage['client_uuid'])) {
            return '';
        }
        return $this->storage['client_uuid'];
    }

    public function setClientUuid(string $clientUuid): void
    {
        $this->storage['client_uuid'] = $clientUuid;
    }

    public function getCostType(): string
    {
        if (!isset($this->storage['cost_type'])) {
            return '';
        }
        return $this->storage['cost_type'];
    }

    public function setCostType(string $costType): void
    {
        $this->storage['cost_type'] = $costType;
    }

    public function getCostLabel(): string
    {
        if (!isset($this->storage['cost_label'])) {
            return '';
        }
        return $this->storage['cost_label'];
    }

    public function setCostLabel(string $costLabel): void
    {
        $this->storage['cost_label'] = $costLabel;
    }

    public function getCostNet(): float
    {
        if (!isset($this->storage['cost_net'])) {
            return '';
        }
        return $this->storage['cost_net'];
    }

    public function setCostNet(float $costNet): void
    {
        $this->storage['cost_net'] = $costNet;
    }

    public function getCostTaxp(): int
    {
        if (!isset($this->storage['cost_taxp'])) {
            return 0;
        }
        return $this->storage['cost_taxp'];
    }

    public function setCostTaxp(int $costTaxp): void
    {
        $this->storage['cost_taxp'] = $costTaxp;
    }

    public function getCostGross(): float
    {
        if (!isset($this->storage['cost_gross'])) {
            return '';
        }
        return $this->storage['cost_gross'];
    }

    public function setCostGross(float $costGross): void
    {
        $this->storage['cost_gross'] = $costGross;
    }

    public function getCostDate(): string
    {
        if (!isset($this->storage['cost_date'])) {
            return '';
        }
        return $this->storage['cost_date'];
    }

    public function setCostDate(string $costDate): void
    {
        $this->storage['cost_date'] = $costDate;
    }

    public function getCostDocno(): string
    {
        if (!isset($this->storage['cost_docno'])) {
            return '';
        }
        return $this->storage['cost_docno'];
    }

    public function setCostDocno(string $costDocno): void
    {
        $this->storage['cost_docno'] = $costDocno;
    }
}
