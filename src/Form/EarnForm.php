<?php

namespace Bitkorn\Cashbook\Form;

use Bitkorn\Trinket\Filter\SanitizeStringFilter;
use Bitkorn\Trinket\Form\AbstractForm;
use Bitkorn\Trinket\Validator\FloatValidator;
use Laminas\Filter\HtmlEntities;
use Laminas\Filter\StringTrim;
use Laminas\Filter\StripTags;
use Laminas\InputFilter\InputFilterProviderInterface;
use Laminas\Validator\Date;
use Laminas\Validator\Digits;
use Laminas\Validator\NotEmpty;
use Laminas\Validator\StringLength;
use Laminas\Validator\Uuid;

class EarnForm extends AbstractForm implements InputFilterProviderInterface
{
    public function init()
    {
        if ($this->primaryKeyAvailable) {
            $this->add(['name' => 'earn_uuid']);
        }
        $this->add(['name' => 'client_uuid']);
        $this->add(['name' => 'earn_label']);
        $this->add(['name' => 'earn_net']);
        $this->add(['name' => 'earn_taxp']);
        $this->add(['name' => 'earn_gross']);
        $this->add(['name' => 'earn_date']);
        $this->add(['name' => 'earn_docno']);
    }

    /**
     * Should return an array specification compatible with
     * {@link \Laminas\InputFilter\Factory::createInputFilter()}.
     * @return array
     */
    public function getInputFilterSpecification()
    {
        $filter = [];

        if ($this->primaryKeyAvailable) {
            $filter['earn_uuid'] = [
                'required' => true,
                'filters' => [
                    ['name' => SanitizeStringFilter::class],
                ], 'validators' => [
                    ['name' => Uuid::class]
                ]
            ];
        }

        $filter['client_uuid'] = [
            'required' => true,
            'filters' => [
                ['name' => SanitizeStringFilter::class],
            ], 'validators' => [
                ['name' => Uuid::class]
            ]
        ];

        $filter['earn_label'] = [
            'required' => true,
            'filters' => [
                ['name' => SanitizeStringFilter::class],
            ], 'validators' => [
                [
                    'name' => StringLength::class,
                    'options' => [
                        'encoding' => 'UTF-8',
                        'min' => 2,
                        'max' => 180,
                    ]
                ]
            ]
        ];

        $filter['earn_net'] = [
            'required' => true,
            'filters' => [
                ['name' => SanitizeStringFilter::class],
            ], 'validators' => [
                ['name' => FloatValidator::class],
                [
                    'name' => NotEmpty::class,
                    'options' => [
                        'type' => NotEmpty::PHP
                    ]
                ]
            ]
        ];

        $filter['earn_taxp'] = [
            'required' => true,
            'filters' => [
                ['name' => SanitizeStringFilter::class],
            ], 'validators' => [
                ['name' => Digits::class]
            ]
        ];

        $filter['earn_gross'] = [
            'required' => true,
            'filters' => [
                ['name' => SanitizeStringFilter::class],
            ], 'validators' => [
                ['name' => FloatValidator::class]
            ]
        ];

        $filter['earn_date'] = [
            'required' => true,
            'filters' => [
                ['name' => SanitizeStringFilter::class],
            ], 'validators' => [
                ['name' => Date::class]
            ]
        ];

        $filter['earn_docno'] = [
            'required' => true,
            'filters' => [
                ['name' => SanitizeStringFilter::class],
            ], 'validators' => [
                [
                    'name' => StringLength::class,
                    'options' => [
                        'encoding' => 'UTF-8',
                        'min' => 1,
                        'max' => 80,
                    ]
                ]
            ]
        ];

        return $filter;
    }
}
