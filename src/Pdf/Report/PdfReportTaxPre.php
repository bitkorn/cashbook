<?php

namespace Bitkorn\Cashbook\Pdf\Report;

use Bitkorn\Cashbook\Pdf\PdfClass;

/**
 * Class PdfReportTaxPre
 * @package Bitkorn\Cashbook\Pdf\Report
 *
 * Umsatzsteuer Voranmeldung
 */
class PdfReportTaxPre extends AbstractPdfReport
{
    protected string $dateFrom;
    protected string $dateTo;
    protected array $earns;
    protected array $earnSums;
    protected array $costs;
    protected array $costSums;

    public function setDateFrom(string $dateFrom): void
    {
        $this->dateFrom = $dateFrom;
    }

    public function setDateTo(string $dateTo): void
    {
        $this->dateTo = $dateTo;
    }

    public function setEarns(array $earns): void
    {
        $this->earns = $earns;
    }

    public function setEarnSums(array $earnSums): void
    {
        $this->earnSums = $earnSums;
    }

    public function setCosts(array $costs): void
    {
        $this->costs = $costs;
    }

    public function setCostSums(array $costSums): void
    {
        $this->costSums = $costSums;
    }

    public function Header()
    {
        $this->SetFontSize($this->fontSizeInitial);
        //$this->ImageSVG($this->storageLocationImg . $this->pathLogo, ($this->getPageWidth() / 2) - ($this->logoWidth / 2), $this->positionTopmost, null, $this->logoHeight);
        //$this->SetY($this->positionTopmost + $this->logoHeight + 1);
        $this->SetY($this->positionTopmost);
        $this->Cell($this->getContentWidth(), 0, $this->clientLabel . ' - ' . $this->clientTaxNo, 'T', 1, 'C', false);
        $this->Cell($this->getContentWidth(), 1, '', 'T', 1, 'C', false);
        $this->contentStart = $this->positionTopmost + $this->logoHeight + 1;
    }

    public function makeDocument(): void
    {
        parent::makeDocument();
        $this->AddPage('P');
        $this->SetFontSize(PdfClass::FONT_SIZE_L);
        $this->SetFont($this->fontFamilyDefault, 'B');
        $this->SetFillColorArray($this->colorArrayWhite);
        $this->SetTextColorArray($this->colorArrayBlack);
        $this->Cell($this->getContentWidth(), 0, $this->dateFrom . ' - ' . $this->dateTo . ' - Ums.-St. Voranmeldung', 0, 1, 'C');
        $this->Ln();
        $this->RowEarnHeader();
        foreach ($this->earns as $earn) {
            $this->RowEarn($earn['earn_label'], $earn['earn_net'], $earn['earn_gross'] - $earn['earn_net'], $earn['earn_gross'], $earn['earn_date'], $earn['earn_docno']);
        }
        $this->RowEarnSum($this->earnSums[0]['sum_net'], $this->earnSums[0]['sum_gross'] - $this->earnSums[0]['sum_net'], $this->earnSums[0]['sum_gross']);
        $this->Ln();
        $this->RowCostHeader();
        foreach ($this->costs as $cost) {
            $this->RowCost($cost['cost_type'], $cost['cost_label'], $cost['cost_net'], $cost['cost_gross'] - $cost['cost_net'], $cost['cost_gross'], $cost['cost_date'], $cost['cost_docno']);
        }
        foreach ($this->costSums as $costSum) {
            $this->RowCostSum($costSum['cost_type'], $costSum['sum_net'], $costSum['sum_gross'] - $costSum['sum_net'], $costSum['sum_gross']);
        }
        $this->Output('Umsatzsteuer_' . $this->dateFrom . '_' . $this->dateTo . '.pdf', 'I');
    }
}
