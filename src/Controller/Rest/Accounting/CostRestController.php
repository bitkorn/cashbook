<?php

namespace Bitkorn\Cashbook\Controller\Rest\Accounting;

use Bitkorn\Cashbook\Entity\CostEntity;
use Bitkorn\Cashbook\Form\CostForm;
use Bitkorn\Cashbook\Service\AccountingService;
use Bitkorn\Trinket\Validator\IsoDate;
use Bitkorn\Trinket\View\Model\JsonModel;
use Bitkorn\User\Controller\AbstractUserRestController;
use Laminas\Http\PhpEnvironment\Response;
use Laminas\Validator\Uuid;

class CostRestController extends AbstractUserRestController
{
    protected AccountingService $accountingService;
    protected CostForm $costForm;

    public function setAccountingService(AccountingService $accountingService): void
    {
        $this->accountingService = $accountingService;
    }

    public function setCostForm(CostForm $costForm): void
    {
        $this->costForm = $costForm;
    }

    /**
     * POST maps to create().
     * @param array $data
     * @return JsonModel
     */
    public function create($data): JsonModel
    {
        $jsonModel = new JsonModel();
        if (!$this->userService->checkUserRoleAccessMin(5)) {
            $this->getResponse()->setStatusCode(Response::STATUS_CODE_403);
            return $jsonModel;
        }
        $this->costForm->init();
        $this->costForm->setData($data);
        if (!$this->costForm->isValid()) {
            $jsonModel->addMessages($this->costForm->getMessages());
            return $jsonModel;
        }
        $costEntity = new CostEntity();
        if ($costEntity->exchangeArrayFromDatabase($this->costForm->getData()) && !empty($uuid = $this->accountingService->insertCost($costEntity))) {
            $jsonModel->setSuccess(1);
            $jsonModel->setUuid($uuid);
        }
        return $jsonModel;
    }

    /**
     * GET
     * @return JsonModel
     */
    public function getList(): JsonModel
    {
        $jsonModel = new JsonModel();
        if (!$this->userService->checkUserRoleAccessMin(5)) {
            $this->getResponse()->setStatusCode(Response::STATUS_CODE_403);
            return $jsonModel;
        }
        $clientUuid = filter_input(INPUT_GET, 'client_uuid', FILTER_SANITIZE_STRING);
        $dateFrom = filter_input(INPUT_GET, 'date_from', FILTER_SANITIZE_STRING);
        $dateTo = filter_input(INPUT_GET, 'date_to', FILTER_SANITIZE_STRING);
        $isoDate = new IsoDate();
        if (!(new Uuid())->isValid($clientUuid) || !$isoDate->isValid($dateFrom) || !$isoDate->isValid($dateTo)) {
            $this->getResponse()->setStatusCode(Response::STATUS_CODE_400);
            return $jsonModel;
        }
        $jsonModel->setArr($this->accountingService->getCostsBetweenDates($clientUuid, $dateFrom, $dateTo));
        $jsonModel->setSuccess(1);
        return $jsonModel;
    }

    /**
     * DELETE maps to delete().
     * @param string $id
     * @return JsonModel
     */
    public function delete($id): JsonModel
    {
        $jsonModel = new JsonModel();
        $this->getResponse()->setStatusCode(Response::STATUS_CODE_501);
        return $jsonModel;
    }

    /**
     * GET
     * @param string $id
     * @return JsonModel
     */
    public function get($id): JsonModel
    {
        $jsonModel = new JsonModel();
        $this->getResponse()->setStatusCode(Response::STATUS_CODE_501);
        return $jsonModel;
    }

    /**
     * PUT maps to update().
     * @param string $id
     * @param array $data
     * @return JsonModel
     */
    public function update($id, $data): JsonModel
    {
        $jsonModel = new JsonModel();
        $this->getResponse()->setStatusCode(Response::STATUS_CODE_501);
        return $jsonModel;
    }
}
