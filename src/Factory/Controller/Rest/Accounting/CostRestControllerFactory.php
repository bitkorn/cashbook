<?php

namespace Bitkorn\Cashbook\Factory\Controller\Rest\Accounting;

use Bitkorn\Cashbook\Controller\Rest\Accounting\CostRestController;
use Bitkorn\Cashbook\Form\CostForm;
use Bitkorn\Cashbook\Service\AccountingService;
use Bitkorn\User\Service\UserService;
use Interop\Container\ContainerInterface;
use Laminas\ServiceManager\Exception\ServiceNotCreatedException;
use Laminas\ServiceManager\Exception\ServiceNotFoundException;
use Laminas\ServiceManager\Factory\FactoryInterface;

class CostRestControllerFactory implements FactoryInterface
{
    /**
     * Create an object
     *
     * @param ContainerInterface $container
     * @param string $requestedName
     * @param null|array $options
     * @return object
     * @throws ServiceNotFoundException if unable to resolve the service
     * @throws ServiceNotCreatedException if an exception is raised when creating a service
     */
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        $controller = new CostRestController();
        $controller->setLogger($container->get('logger'));
        $controller->setUserService($container->get(UserService::class));
        $controller->setAccountingService($container->get(AccountingService::class));
        $controller->setCostForm($container->get(CostForm::class));
        return $controller;
    }
}
