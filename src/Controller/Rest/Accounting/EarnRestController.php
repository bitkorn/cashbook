<?php

namespace Bitkorn\Cashbook\Controller\Rest\Accounting;

use Bitkorn\Cashbook\Entity\EarnEntity;
use Bitkorn\Cashbook\Form\EarnForm;
use Bitkorn\Cashbook\Service\AccountingService;
use Bitkorn\Trinket\Validator\IsoDate;
use Bitkorn\Trinket\View\Model\JsonModel;
use Bitkorn\User\Controller\AbstractUserRestController;
use Laminas\Http\Response;
use Laminas\Validator\Uuid;

class EarnRestController extends AbstractUserRestController
{
    protected AccountingService $accountingService;
    protected EarnForm $earnForm;

    public function setAccountingService(AccountingService $accountingService): void
    {
        $this->accountingService = $accountingService;
    }

    public function setEarnForm(EarnForm $earnForm): void
    {
        $this->earnForm = $earnForm;
    }

    /**
     * POST maps to create().
     * @param array $data
     * @return JsonModel
     */
    public function create($data): JsonModel
    {
        $jsonModel = new JsonModel();
        if (!$this->userService->checkUserRoleAccessMin(5)) {
            $this->getResponse()->setStatusCode(Response::STATUS_CODE_403);
            return $jsonModel;
        }
        $this->earnForm->init();
        $this->earnForm->setData($data);
        if (!$this->earnForm->isValid()) {
            $jsonModel->addMessages($this->earnForm->getMessages());
            return $jsonModel;
        }
        $earnEntity = new EarnEntity();
        if ($earnEntity->exchangeArrayFromDatabase($this->earnForm->getData()) && !empty($uuid = $this->accountingService->insertEarn($earnEntity))) {
            $jsonModel->setSuccess(1);
            $jsonModel->setUuid($uuid);
        }
        return $jsonModel;
    }

    /**
     * GET
     * @return JsonModel
     */
    public function getList(): JsonModel
    {
        $jsonModel = new JsonModel();
        if (!$this->userService->checkUserRoleAccessMin(5)) {
            $this->getResponse()->setStatusCode(Response::STATUS_CODE_403);
            return $jsonModel;
        }
        $clientUuid = filter_input(INPUT_GET, 'client_uuid', FILTER_SANITIZE_STRING);
        $dateFrom = filter_input(INPUT_GET, 'date_from', FILTER_SANITIZE_STRING);
        $dateTo = filter_input(INPUT_GET, 'date_to', FILTER_SANITIZE_STRING);
        $isoDate = new IsoDate();
        if (!(new Uuid())->isValid($clientUuid) || !$isoDate->isValid($dateFrom) || !$isoDate->isValid($dateTo)) {
            $this->getResponse()->setStatusCode(Response::STATUS_CODE_400);
            return $jsonModel;
        }
        $jsonModel->setArr($this->accountingService->getEarnsBetweenDates($clientUuid, $dateFrom, $dateTo));
        $jsonModel->setSuccess(1);
        return $jsonModel;
    }
}
