<?php

namespace Bitkorn\Cashbook\Table;

use Bitkorn\Cashbook\Entity\EarnEntity;
use Bitkorn\Trinket\Table\AbstractLibTable;
use Laminas\Db\ResultSet\HydratingResultSet;
use Laminas\Db\Sql\Delete;
use Laminas\Db\Sql\Expression;
use Laminas\Db\Sql\Select;
use Laminas\Db\Sql\Update;
use Laminas\Db\Sql\Where;

class EarnTable extends AbstractLibTable
{
    /** @var string */
    protected $table = 'earn';

    /**
     * @param string $earnUuid
     * @param string $clientUuid
     * @return array
     */
    public function getEarn(string $earnUuid, string $clientUuid): array
    {
        $select = $this->sql->select();
        try {
            $select->where(['earn_uuid' => $earnUuid, 'client_uuid' => $clientUuid]);
            /** @var HydratingResultSet $result */
            $result = $this->selectWith($select);
            if ($result->valid() && $result->count() == 1) {
                return $result->toArray()[0];
            }
        } catch (\Exception $exception) {
            $this->log($exception, __CLASS__, __FUNCTION__);
        }
        return [];
    }

    public function getEarns(string $clientUuid): array
    {
        $select = $this->sql->select();
        try {
            $select->where(['client_uuid' => $clientUuid]);
            /** @var HydratingResultSet $result */
            $result = $this->selectWith($select);
            if ($result->valid() && $result->count() == 1) {
                return $result->toArray()[0];
            }
        } catch (\Exception $exception) {
            $this->log($exception, __CLASS__, __FUNCTION__);
        }
        return [];
    }

    /**
     * @param EarnEntity $earnEntity
     * @return string
     */
    public function insertEarn(EarnEntity $earnEntity): string
    {
        $insert = $this->sql->insert();
        $uuid = $this->uuid();
        try {
            $insert->values([
                'earn_uuid'   => $uuid,
                'client_uuid' => $earnEntity->getClientUuid(),
                'earn_label'  => $earnEntity->getEarnLabel(),
                'earn_net'    => $earnEntity->getEarnNet(),
                'earn_taxp'   => $earnEntity->getEarnTaxp(),
                'earn_gross'  => $earnEntity->getEarnGross(),
                'earn_date'   => $earnEntity->getEarnDate(),
                'earn_docno'  => $earnEntity->getEarnDocno(),
            ]);
            if ($this->insertWith($insert) > 0) {
                return $uuid;
            }
        } catch (\Exception $exception) {
            $this->log($exception, __CLASS__, __FUNCTION__);
        }
        return '';
    }

    public function getEarnsBetweenDates(string $clientUuid, string $dateFrom, string $dateTo): array
    {
        $select = $this->sql->select();
        try {
            $select->where(['client_uuid' => $clientUuid]);
            $select->where->between('earn_date', $dateFrom, $dateTo);
            $select->order('earn_date');
            /** @var HydratingResultSet $result */
            $result = $this->selectWith($select);
            if ($result->valid() && $result->count() > 0) {
                return $result->toArray();
            }
        } catch (\Exception $exception) {
            $this->log($exception, __CLASS__, __FUNCTION__);
        }
        return [];
    }

    public function getEarnSumsBetweenDates(string $clientUuid, string $dateFrom, string $dateTo): array
    {
        $select = $this->sql->select();
        try {
            $select->columns([
                'sum_net'   => new Expression('COALESCE(SUM(earn_net), 0)'),
                'sum_gross' => new Expression('COALESCE(SUM(earn_gross), 0)'),
            ]);
            $select->where(['client_uuid' => $clientUuid]);
            $select->where->between('earn_date', $dateFrom, $dateTo);
            /** @var HydratingResultSet $result */
            $result = $this->selectWith($select);
            if ($result->valid() && $result->count() > 0) {
                return $result->toArray();
            }
        } catch (\Exception $exception) {
            $this->log($exception, __CLASS__, __FUNCTION__);
        }
        return [];
    }
}
