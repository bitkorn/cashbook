<?php

namespace Bitkorn\Cashbook\Table;

use Bitkorn\Trinket\Table\AbstractLibTable;
use Laminas\Db\ResultSet\HydratingResultSet;
use Laminas\Db\Sql\Delete;
use Laminas\Db\Sql\Expression;
use Laminas\Db\Sql\Select;
use Laminas\Db\Sql\Update;
use Laminas\Db\Sql\Where;

class ClientTable extends AbstractLibTable
{
    /** @var string */
    protected $table = 'client';

    /**
     * @param string $clientUuid
     * @return array
     */
    public function getClient(string $clientUuid): array
    {
        $select = $this->sql->select();
        try {
            $select->where(['client_uuid' => $clientUuid]);
            /** @var HydratingResultSet $result */
            $result = $this->selectWith($select);
            if ($result->valid() && $result->count() == 1) {
                return $result->toArray()[0];
            }
        } catch (\Exception $exception) {
            $this->log($exception, __CLASS__, __FUNCTION__);
        }
        return [];
    }

    public function getClientUuidAssoc(): array
    {
        $select = $this->sql->select();
        $assoc = [];
        try {
            /** @var HydratingResultSet $result */
            $result = $this->selectWith($select);
            if ($result->valid() && $result->count() > 0) {
                $res = $result->toArray();
                foreach ($res as $row) {
                    $assoc[$row['client_uuid']] = $row['client_label'];
                }
            }
        } catch (\Exception $exception) {
            $this->log($exception, __CLASS__, __FUNCTION__);
        }
        return $assoc;
    }
}
