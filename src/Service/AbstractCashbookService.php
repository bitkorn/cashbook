<?php

namespace Bitkorn\Cashbook\Service;

use Bitkorn\Cashbook\Table\ClientTable;
use Bitkorn\Cashbook\Table\CostTable;
use Bitkorn\Cashbook\Table\EarnTable;
use Bitkorn\Cashbook\Table\TaxPreTable;
use Bitkorn\Cashbook\Table\TaxReTable;
use Bitkorn\Trinket\Service\AbstractService;

abstract class AbstractCashbookService extends AbstractService
{
    protected ClientTable $clientTable;
    protected CostTable $costTable;
    protected EarnTable $earnTable;
    protected TaxPreTable $taxPreTable;
    protected TaxReTable $taxReTable;
    protected array $costTypes;

    public function setClientTable(ClientTable $clientTable): void
    {
        $this->clientTable = $clientTable;
    }

    public function setCostTable(CostTable $costTable): void
    {
        $this->costTable = $costTable;
    }

    public function setEarnTable(EarnTable $earnTable): void
    {
        $this->earnTable = $earnTable;
    }

    public function setTaxPreTable(TaxPreTable $taxPreTable): void
    {
        $this->taxPreTable = $taxPreTable;
    }

    public function setTaxReTable(TaxReTable $taxReTable): void
    {
        $this->taxReTable = $taxReTable;
    }

    public function setCostTypes(array $costTypes): void
    {
        $this->costTypes = $costTypes;
    }

    public function getCostTypes(): array
    {
        return $this->costTypes;
    }

}
