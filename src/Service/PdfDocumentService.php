<?php

namespace Bitkorn\Cashbook\Service;

use Bitkorn\Cashbook\Pdf\Report\PdfReportTaxPre;

class PdfDocumentService extends AbstractCashbookService
{
    protected PdfReportTaxPre $pdfReportTaxPre;

    public function setPdfReportTaxPre(PdfReportTaxPre $pdfReportTaxPre): void
    {
        $this->pdfReportTaxPre = $pdfReportTaxPre;
    }

    /**
     * @param string $clientUuid
     * @param string $dateFrom
     * @param string $dateTo
     */
    public function createPdfReportTaxPre(string $clientUuid, string $dateFrom, string $dateTo): void
    {
        $this->pdfReportTaxPre->setDateFrom($dateFrom);
        $this->pdfReportTaxPre->setDateTo($dateTo);
        $this->pdfReportTaxPre->setEarns($this->earnTable->getEarnsBetweenDates($clientUuid, $dateFrom, $dateTo));
        $this->pdfReportTaxPre->setEarnSums($this->earnTable->getEarnSumsBetweenDates($clientUuid, $dateFrom, $dateTo));
        $this->pdfReportTaxPre->setCosts($this->costTable->getCostsBetweenDates($clientUuid, $dateFrom, $dateTo));
        $this->pdfReportTaxPre->setCostSums($this->costTable->getCostSumsBetweenDates($clientUuid, $dateFrom, $dateTo));
        $client = $this->clientTable->getClient($clientUuid);
        $this->pdfReportTaxPre->setClientLabel($client['client_label']);
        $this->pdfReportTaxPre->setClientTaxNo($client['client_tax_no']);
        $this->pdfReportTaxPre->makeDocument();
    }
}
