<?php

namespace Bitkorn\Cashbook\Factory\Service;

use Bitkorn\Cashbook\Service\AccountingService;
use Bitkorn\Cashbook\Table\ClientTable;
use Bitkorn\Cashbook\Table\CostTable;
use Bitkorn\Cashbook\Table\EarnTable;
use Bitkorn\Cashbook\Table\TaxPreTable;
use Bitkorn\Cashbook\Table\TaxReTable;
use Bitkorn\Trinket\Table\ToolsTable;
use Interop\Container\ContainerInterface;
use Laminas\ServiceManager\Exception\ServiceNotCreatedException;
use Laminas\ServiceManager\Exception\ServiceNotFoundException;
use Laminas\ServiceManager\Factory\FactoryInterface;

class AccountingServiceFactory implements FactoryInterface
{
    /**
     * Create an object
     *
     * @param ContainerInterface $container
     * @param string $requestedName
     * @param null|array $options
     * @return object
     * @throws ServiceNotFoundException if unable to resolve the service
     * @throws ServiceNotCreatedException if an exception is raised when creating a service
     */
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        $service = new AccountingService();
        $service->setLogger($container->get('logger'));
        $service->setClientTable($container->get(ClientTable::class));
        $service->setCostTable($container->get(CostTable::class));
        $service->setEarnTable($container->get(EarnTable::class));
        $service->setTaxPreTable($container->get(TaxPreTable::class));
        $service->setTaxReTable($container->get(TaxReTable::class));
        /** @var ToolsTable $tt */
        $tt = $container->get(ToolsTable::class);
        $service->setCostTypes($tt->getEnumValuesPostgreSQL('enum_cost_type'));
        return $service;
    }
}
