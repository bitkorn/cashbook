<?php

namespace Bitkorn\Cashbook\Entity;

use Bitkorn\Trinket\Entity\AbstractEntity;

class EarnEntity extends AbstractEntity
{
	public array $mapping = [
		'earn_uuid' => 'earn_uuid',
		'client_uuid' => 'client_uuid',
		'earn_label' => 'earn_label',
		'earn_net' => 'earn_net',
		'earn_taxp' => 'earn_taxp',
		'earn_gross' => 'earn_gross',
		'earn_date' => 'earn_date',
		'earn_docno' => 'earn_docno',
	];

	protected $primaryKey = 'earn_uuid';

	public function getEarnUuid(): string
	{
		if(!isset($this->storage['earn_uuid'])) {
			return '';
		}
		return $this->storage['earn_uuid'];
	}

	public function setEarnUuid(string $earnUuid): void
	{
		$this->storage['earn_uuid'] = $earnUuid;
	}

	public function getClientUuid(): string
	{
		if(!isset($this->storage['client_uuid'])) {
			return '';
		}
		return $this->storage['client_uuid'];
	}

	public function setClientUuid(string $clientUuid): void
	{
		$this->storage['client_uuid'] = $clientUuid;
	}

	public function getEarnLabel(): string
	{
		if(!isset($this->storage['earn_label'])) {
			return '';
		}
		return $this->storage['earn_label'];
	}

	public function setEarnLabel(string $earnLabel): void
	{
		$this->storage['earn_label'] = $earnLabel;
	}

	public function getEarnNet(): float
	{
		if(!isset($this->storage['earn_net'])) {
			return '';
		}
		return $this->storage['earn_net'];
	}

	public function setEarnNet(float $earnNet): void
	{
		$this->storage['earn_net'] = $earnNet;
	}

	public function getEarnTaxp(): int
	{
		if(!isset($this->storage['earn_taxp'])) {
			return 0;
		}
		return $this->storage['earn_taxp'];
	}

	public function setEarnTaxp(int $earnTaxp): void
	{
		$this->storage['earn_taxp'] = $earnTaxp;
	}

	public function getEarnGross(): float
	{
		if(!isset($this->storage['earn_gross'])) {
			return '';
		}
		return $this->storage['earn_gross'];
	}

	public function setEarnGross(float $earnGross): void
	{
		$this->storage['earn_gross'] = $earnGross;
	}

	public function getEarnDate(): string
	{
		if(!isset($this->storage['earn_date'])) {
			return '';
		}
		return $this->storage['earn_date'];
	}

	public function setEarnDate(string $earnDate): void
	{
		$this->storage['earn_date'] = $earnDate;
	}

	public function getEarnDocno(): string
	{
		if(!isset($this->storage['earn_docno'])) {
			return '';
		}
		return $this->storage['earn_docno'];
	}

	public function setEarnDocno(string $earnDocno): void
	{
		$this->storage['earn_docno'] = $earnDocno;
	}
}
