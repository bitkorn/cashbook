<?php

namespace Bitkorn\Cashbook\Factory\Form;

use Bitkorn\Cashbook\Form\CostForm;
use Bitkorn\Trinket\Table\ToolsTable;
use Interop\Container\ContainerInterface;
use Laminas\ServiceManager\Exception\ServiceNotCreatedException;
use Laminas\ServiceManager\Exception\ServiceNotFoundException;
use Laminas\ServiceManager\Factory\FactoryInterface;

class CostFormFactory implements FactoryInterface
{
    /**
     * Create an object
     *
     * @param ContainerInterface $container
     * @param string $requestedName
     * @param null|array $options
     * @return object
     * @throws ServiceNotFoundException if unable to resolve the service
     * @throws ServiceNotCreatedException if an exception is raised when creating a service
     */
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        $form = new CostForm();
        /** @var ToolsTable $tt */
        $tt = $container->get(ToolsTable::class);
        $form->setCostTypes($tt->getEnumValuesPostgreSQL('enum_cost_type'));
        return $form;
    }
}
