<?php

namespace Bitkorn\Cashbook\Table;

use Bitkorn\Cashbook\Entity\CostEntity;
use Bitkorn\Cashbook\Entity\EarnEntity;
use Bitkorn\Trinket\Table\AbstractLibTable;
use Laminas\Db\ResultSet\HydratingResultSet;
use Laminas\Db\Sql\Delete;
use Laminas\Db\Sql\Expression;
use Laminas\Db\Sql\Select;
use Laminas\Db\Sql\Update;
use Laminas\Db\Sql\Where;

class CostTable extends AbstractLibTable
{
    /** @var string */
    protected $table = 'cost';

    /**
     * @param string $costUuid
     * @return array
     */
    public function getCost(string $costUuid): array
    {
        $select = $this->sql->select();
        try {
            $select->where(['cost_uuid' => $costUuid]);
            /** @var HydratingResultSet $result */
            $result = $this->selectWith($select);
            if ($result->valid() && $result->count() == 1) {
                return $result->toArray()[0];
            }
        } catch (\Exception $exception) {
            $this->log($exception, __CLASS__, __FUNCTION__);
        }
        return [];
    }

    public function getCostsByType(string $clientUuid, string $costType): array
    {
        $select = $this->sql->select();
        try {
            $select->where(['client_uuid' => $clientUuid, 'cost_type' => $costType]);
            /** @var HydratingResultSet $result */
            $result = $this->selectWith($select);
            if ($result->valid() && $result->count() == 1) {
                return $result->toArray()[0];
            }
        } catch (\Exception $exception) {
            $this->log($exception, __CLASS__, __FUNCTION__);
        }
        return [];
    }

    public function insertCost(CostEntity $costEntity): string
    {
        $insert = $this->sql->insert();
        $uuid = $this->uuid();
        try {
            $insert->values([
                'cost_uuid'   => $uuid,
                'client_uuid' => $costEntity->getClientUuid(),
                'cost_type'   => $costEntity->getCostType(),
                'cost_label'  => $costEntity->getCostLabel(),
                'cost_net'    => $costEntity->getCostNet(),
                'cost_taxp'   => $costEntity->getCostTaxp(),
                'cost_gross'  => $costEntity->getCostGross(),
                'cost_date'   => $costEntity->getCostDate(),
                'cost_docno'  => $costEntity->getCostDocno(),
            ]);
            if ($this->insertWith($insert) > 0) {
                return $uuid;
            }
        } catch (\Exception $exception) {
            $this->log($exception, __CLASS__, __FUNCTION__);
        }
        return '';
    }

    public function getCostsBetweenDates(string $clientUuid, string $dateFrom, string $dateTo): array
    {
        $select = $this->sql->select();
        try {
            $select->where(['client_uuid' => $clientUuid]);
            $select->where->between('cost_date', $dateFrom, $dateTo);
            $select->order('cost_date');
            /** @var HydratingResultSet $result */
            $result = $this->selectWith($select);
            if ($result->valid() && $result->count() > 0) {
                return $result->toArray();
            }
        } catch (\Exception $exception) {
            $this->log($exception, __CLASS__, __FUNCTION__);
        }
        return [];
    }

    public function getCostSumsBetweenDates(string $clientUuid, string $dateFrom, string $dateTo): array
    {
        $select = $this->sql->select();
        try {
            $select->columns(['cost_type',
                'sum_net'   => new Expression('COALESCE(SUM(cost_net), 0)'),
                'sum_gross' => new Expression('COALESCE(SUM(cost_gross), 0)'),
            ]);
            $select->where(['client_uuid' => $clientUuid]);
            $select->where->between('cost_date', $dateFrom, $dateTo);
            $select->group('cost_type');
            /** @var HydratingResultSet $result */
            $result = $this->selectWith($select);
            if ($result->valid() && $result->count() > 0) {
                return $result->toArray();
            }
        } catch (\Exception $exception) {
            $this->log($exception, __CLASS__, __FUNCTION__);
        }
        return [];
    }
}
