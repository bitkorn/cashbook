<?php

namespace Bitkorn\Cashbook\Pdf\Report;

use Bitkorn\Cashbook\Pdf\PdfClass;
use Bitkorn\Cashbook\Pdf\PdfHeaderFooterSimple;

abstract class AbstractPdfReport extends PdfHeaderFooterSimple
{
    protected string $clientLabel;
    protected string $clientTaxNo;

    public function setClientLabel(string $clientLabel): void
    {
        $this->clientLabel = $clientLabel;
    }

    public function setClientTaxNo(string $clientTaxNo): void
    {
        $this->clientTaxNo = $clientTaxNo;
    }

    protected int $rowEarn_c1_w = 60;
    protected int $rowEarn_c2_w = 20;
    protected int $rowEarn_c3_w = 20;
    protected int $rowEarn_c4_w = 20;
    protected int $rowEarn_c5_w = 20;
    protected int $rowEarn_c6_w = 40;

    protected int $rowCost_c1_w = 10;
    protected int $rowCost_c2_w = 50;
    protected int $rowCost_c3_w = 20;
    protected int $rowCost_c4_w = 20;
    protected int $rowCost_c5_w = 20;
    protected int $rowCost_c6_w = 20;
    protected int $rowCost_c7_w = 40;

    /**
     * @return float Normally, it is 180.
     */
    protected function getRowEarnColsWidth(): float
    {
        return $this->rowEarn_c1_w + $this->rowEarn_c2_w + $this->rowEarn_c3_w + $this->rowEarn_c4_w + $this->rowEarn_c5_w + $this->rowEarn_c6_w;
    }

    protected function RowEarnHeader(): void
    {
        $this->SetFontSize(PdfClass::FONT_SIZE_M);
        $this->SetFillColorArray($this->colorArrayBrand);
        $this->SetTextColorArray($this->colorArrayWhite);
        $this->SetFont($this->fontFamilyDefault, 'B');
        $this->Cell($this->getRowEarnColsWidth(), 0, 'Einnahmen', 0, 1, 'C', true);
        $this->Cell($this->rowEarn_c1_w, 0, 'Label', 0, 0, 'L', true);
        $this->Cell($this->rowEarn_c2_w, 0, 'Netto', 0, 0, 'R', true);
        $this->Cell($this->rowEarn_c3_w, 0, 'Ums.-St.', 0, 0, 'R', true);
        $this->Cell($this->rowEarn_c4_w, 0, 'Brutto', 0, 0, 'R', true);
        $this->Cell($this->rowEarn_c5_w, 0, 'Datum', 0, 0, 'R', true);
        $this->Cell($this->rowEarn_c6_w, 0, 'Beleg', 0, 1, 'R', true);
    }

    protected function RowEarn(string $label, string $net, string $tax, string $gross, string $date, string $docNo): void
    {
        $this->SetFontSize(PdfClass::FONT_SIZE_XS);
        $this->SetFillColorArray($this->colorArrayWhite);
        $this->SetTextColorArray($this->colorArrayBlack);
        $this->SetFont($this->fontFamilyDefault, '');
        $this->Cell($this->rowEarn_c1_w, 0, $label, 0, 0, 'L', false);
        $this->Cell($this->rowEarn_c2_w, 0, $this->numberFormatService->format($net), 0, 0, 'R', false);
        $this->Cell($this->rowEarn_c3_w, 0, $this->numberFormatService->format($tax), 0, 0, 'R', false);
        $this->Cell($this->rowEarn_c4_w, 0, $this->numberFormatService->format($gross), 0, 0, 'R', false);
        $this->Cell($this->rowEarn_c5_w, 0, $date, 0, 0, 'R', false);
        $this->Cell($this->rowEarn_c6_w, 0, $docNo, 0, 1, 'R', false);
    }

    protected function RowEarnSum(string $net, string $tax, string $gross): void
    {
        $this->SetFontSize(PdfClass::FONT_SIZE_S);
        $this->SetFillColorArray($this->colorArrayWhite);
        $this->SetTextColorArray($this->colorArrayBlack);
        $this->SetFont($this->fontFamilyDefault, 'B');
        $this->Cell($this->rowEarn_c1_w, 0, '', 0, 0, 'L', false);
        $this->Cell($this->rowEarn_c2_w, 0, $this->numberFormatService->format($net), 'T', 0, 'R', false);
        $this->Cell($this->rowEarn_c3_w, 0, $this->numberFormatService->format($tax), 'T', 0, 'R', false);
        $this->Cell($this->rowEarn_c4_w, 0, $this->numberFormatService->format($gross), 'T', 0, 'R', false);
        $this->Cell($this->rowEarn_c5_w + $this->rowEarn_c6_w, 0, '', 0, 1);
    }

    /**
     * @return float Normally, it is 180.
     */
    protected function getRowCostColsWidth(): float
    {
        return $this->rowCost_c1_w + $this->rowCost_c2_w + $this->rowCost_c3_w + $this->rowCost_c4_w + $this->rowCost_c5_w + $this->rowCost_c6_w + $this->rowCost_c7_w;
    }

    protected function RowCostHeader(): void
    {
        $this->SetFontSize(PdfClass::FONT_SIZE_M);
        $this->SetFillColorArray($this->colorArrayBrand);
        $this->SetTextColorArray($this->colorArrayWhite);
        $this->SetFont($this->fontFamilyDefault, 'B');
        $this->Cell($this->getRowCostColsWidth(), 0, 'Ausgaben', 0, 1, 'C', true);
        $this->Cell($this->rowCost_c1_w, 0, 'Typ', 0, 0, 'L', true);
        $this->Cell($this->rowCost_c2_w, 0, 'Label', 0, 0, 'L', true);
        $this->Cell($this->rowCost_c3_w, 0, 'Netto', 0, 0, 'R', true);
        $this->Cell($this->rowCost_c4_w, 0, 'Ums.-St.', 0, 0, 'R', true);
        $this->Cell($this->rowCost_c5_w, 0, 'Brutto', 0, 0, 'R', true);
        $this->Cell($this->rowCost_c6_w, 0, 'Datum', 0, 0, 'R', true);
        $this->Cell($this->rowCost_c7_w, 0, 'Beleg', 0, 1, 'R', true);
    }

    protected function RowCost(string $type, string $label, string $net, string $tax, string $gross, string $date, string $docNo): void
    {
        $this->SetFontSize(PdfClass::FONT_SIZE_XS);
        $this->SetFillColorArray($this->colorArrayWhite);
        $this->SetTextColorArray($this->colorArrayBlack);
        $this->SetFont($this->fontFamilyDefault, '');
        $this->Cell($this->rowCost_c1_w, 0, $type, 0, 0, 'L', false);
        $this->Cell($this->rowCost_c2_w, 0, $label, 0, 0, 'L', false);
        $this->Cell($this->rowCost_c3_w, 0, $this->numberFormatService->format($net), 0, 0, 'R', false);
        $this->Cell($this->rowCost_c4_w, 0, $this->numberFormatService->format($tax), 0, 0, 'R', false);
        $this->Cell($this->rowCost_c5_w, 0, $this->numberFormatService->format($gross), 0, 0, 'R', false);
        $this->Cell($this->rowCost_c6_w, 0, $date, 0, 0, 'R', false);
        $this->Cell($this->rowCost_c7_w, 0, $docNo, 0, 1, 'R', false);
    }

    protected function RowCostSum(string $type, string $net, string $tax, string $gross): void
    {
        $this->SetFontSize(PdfClass::FONT_SIZE_S);
        $this->SetFillColorArray($this->colorArrayWhite);
        $this->SetTextColorArray($this->colorArrayBlack);
        $this->SetFont($this->fontFamilyDefault, 'B');
        $this->Cell($this->rowCost_c1_w, 0, $type, 0, 0, 'L', false);
        $this->Cell($this->rowCost_c2_w, 0, '', 0, 0, 'L', false);
        $this->Cell($this->rowCost_c3_w, 0, $this->numberFormatService->format($net), 'T', 0, 'R', false);
        $this->Cell($this->rowCost_c4_w, 0, $this->numberFormatService->format($tax), 'T', 0, 'R', false);
        $this->Cell($this->rowCost_c5_w, 0, $this->numberFormatService->format($gross), 'T', 0, 'R', false);
        $this->Cell($this->rowCost_c6_w + $this->rowCost_c7_w, 0, '', 0, 1);
    }
}
