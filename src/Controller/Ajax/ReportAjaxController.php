<?php

namespace Bitkorn\Cashbook\Controller\Ajax;

use Bitkorn\Cashbook\Service\AccountingService;
use Bitkorn\Cashbook\Service\PdfDocumentService;
use Bitkorn\Trinket\Validator\IsoDate;
use Bitkorn\Trinket\View\Model\JsonModel;
use Bitkorn\User\Controller\AbstractUserController;
use Laminas\Http\Response;
use Laminas\Validator\Uuid;

class ReportAjaxController extends AbstractUserController
{
    protected AccountingService $accountingService;
    protected PdfDocumentService $pdfDocumentService;

    public function setAccountingService(AccountingService $accountingService): void
    {
        $this->accountingService = $accountingService;
    }

    public function setPdfDocumentService(PdfDocumentService $pdfDocumentService): void
    {
        $this->pdfDocumentService = $pdfDocumentService;
    }

    /**
     * @return JsonModel
     */
    public function createReportAction(): JsonModel
    {
        $jsonModel = new JsonModel();
        if (!$this->userService->checkUserRoleAccessMin(5)) {
            $this->getResponse()->setStatusCode(Response::STATUS_CODE_403);
            return $jsonModel;
        }
        $clientUuid = filter_input(INPUT_GET, 'client_uuid', FILTER_SANITIZE_STRING);
        $dateFrom = filter_input(INPUT_GET, 'date_from', FILTER_SANITIZE_STRING);
        $dateTo = filter_input(INPUT_GET, 'date_to', FILTER_SANITIZE_STRING);
        $isoDate = new IsoDate();
        if (!(new Uuid())->isValid($clientUuid) || !$isoDate->isValid($dateFrom) || !$isoDate->isValid($dateTo)) {
            $this->getResponse()->setStatusCode(Response::STATUS_CODE_400);
            return $jsonModel;
        }
        $sums = [];
        $sums['list_earn'] = $this->accountingService->getEarnsBetweenDates($clientUuid, $dateFrom, $dateTo);
        $sums['list_cost'] = $this->accountingService->getCostsBetweenDates($clientUuid, $dateFrom, $dateTo);
        $sums['sum_earn'] = $this->accountingService->getEarnSumsBetweenDates($clientUuid, $dateFrom, $dateTo);
        $sums['sum_cost'] = $this->accountingService->getCostSumsBetweenDates($clientUuid, $dateFrom, $dateTo);
        $jsonModel->setObj($sums);
        $jsonModel->setSuccess(1);
        return $jsonModel;
    }

    /**
     * Umsatzsteuer Voranmeldung
     *
     * @return JsonModel
     */
    public function createPdfReportTaxPreAction(): JsonModel
    {
        $jsonModel = new JsonModel();
        $this->userService->setSessionHashManually($this->params()->fromQuery('hash'));
        if (!$this->userService->checkUserRoleAccessMin(5)) {
            $this->getResponse()->setStatusCode(Response::STATUS_CODE_403);
            return $jsonModel;
        }
        $clientUuid = filter_input(INPUT_GET, 'client_uuid', FILTER_SANITIZE_STRING);
        $dateFrom = filter_input(INPUT_GET, 'date_from', FILTER_SANITIZE_STRING);
        $dateTo = filter_input(INPUT_GET, 'date_to', FILTER_SANITIZE_STRING);
        $isoDate = new IsoDate();
        if (!(new Uuid())->isValid($clientUuid) || !$isoDate->isValid($dateFrom) || !$isoDate->isValid($dateTo)) {
            $this->getResponse()->setStatusCode(Response::STATUS_CODE_400);
            return $jsonModel;
        }
        $this->pdfDocumentService->createPdfReportTaxPre($clientUuid, $dateFrom, $dateTo);
        $jsonModel->setSuccess(1);
        return $jsonModel;
    }
}
