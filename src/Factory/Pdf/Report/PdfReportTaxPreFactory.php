<?php

namespace Bitkorn\Cashbook\Factory\Pdf\Report;

use Bitkorn\Cashbook\Pdf\Report\PdfReportTaxPre;
use Bitkorn\Trinket\Service\I18n\NumberFormatService;
use Interop\Container\ContainerInterface;
use Laminas\ServiceManager\Factory\FactoryInterface;

class PdfReportTaxPreFactory implements FactoryInterface
{

    /**
     * @inheritDoc
     */
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        $pdf = new PdfReportTaxPre();
        $pdf->setNumberFormatService($container->get(NumberFormatService::class));
        $config = $container->get('config')['bitkorn_cashbook'];
        $pdf->setProtectPasswd($config['protect_passwd']);
        $pdf->setStorageLocationImg($config['path_img']);
        $pdf->setStorageLocationPdf($config['path_pdf']);
        $pdf->setColorArrayBrand($config['brand_rgb']);
        $pdf->setPathLogo($config['image_paths']['logo']);
        $pdf->SetCreator($config['data_creator']);
        $pdf->SetAuthor($config['data_author']);
        return $pdf;
    }
}
