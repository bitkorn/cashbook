<?php

namespace Bitkorn\Cashbook\Form;

use Bitkorn\Trinket\Filter\SanitizeStringFilter;
use Bitkorn\Trinket\Form\AbstractForm;
use Bitkorn\Trinket\Validator\FloatValidator;
use Laminas\Filter\HtmlEntities;
use Laminas\Filter\StringTrim;
use Laminas\Filter\StripTags;
use Laminas\InputFilter\InputFilterProviderInterface;
use Laminas\Validator\Date;
use Laminas\Validator\Digits;
use Laminas\Validator\InArray;
use Laminas\Validator\NotEmpty;
use Laminas\Validator\StringLength;
use Laminas\Validator\Uuid;

class CostForm extends AbstractForm implements InputFilterProviderInterface
{
    protected array $costTypes;

    public function setCostTypes(array $costTypes): void
    {
        $this->costTypes = $costTypes;
    }

    public function init()
    {
        if ($this->primaryKeyAvailable) {
            $this->add(['name' => 'cost_uuid']);
        }
        $this->add(['name' => 'client_uuid']);
        $this->add(['name' => 'cost_type']);
        $this->add(['name' => 'cost_label']);
        $this->add(['name' => 'cost_net']);
        $this->add(['name' => 'cost_taxp']);
        $this->add(['name' => 'cost_gross']);
        $this->add(['name' => 'cost_date']);
        $this->add(['name' => 'cost_docno']);
    }

    /**
     * Should return an array specification compatible with
     * {@link \Laminas\InputFilter\Factory::createInputFilter()}.
     * @return array
     */
    public function getInputFilterSpecification()
    {
        $filter = [];

        if ($this->primaryKeyAvailable) {
            $filter['cost_uuid'] = [
                'required'      => true,
                'filters'       => [
                    ['name' => SanitizeStringFilter::class],
                ], 'validators' => [
                    ['name' => Uuid::class]
                ]
            ];
        }

        $filter['client_uuid'] = [
            'required'      => true,
            'filters'       => [
                ['name' => SanitizeStringFilter::class],
            ], 'validators' => [
                ['name' => Uuid::class]
            ]
        ];

        $filter['cost_type'] = [
            'required'      => true,
            'filters'       => [
                ['name' => SanitizeStringFilter::class],
            ], 'validators' => [
                [
                    'name'    => InArray::class,
                    'options' => ['haystack' => $this->costTypes]

                ]
            ]
        ];

        $filter['cost_label'] = [
            'required'      => true,
            'filters'       => [
                ['name' => SanitizeStringFilter::class],
            ], 'validators' => [
                [
                    'name'    => StringLength::class,
                    'options' => [
                        'encoding' => 'UTF-8',
                        'min'      => 2,
                        'max'      => 180,
                    ]
                ]
            ]
        ];

        $filter['cost_net'] = [
            'required'      => true,
            'filters'       => [
                ['name' => SanitizeStringFilter::class],
            ], 'validators' => [
                ['name' => FloatValidator::class],
                [
                    'name' => NotEmpty::class,
                    'options' => [
                        'type' => NotEmpty::PHP
                    ]
                ]
            ]
        ];

        $filter['cost_taxp'] = [
            'required'      => true,
            'filters'       => [
                ['name' => SanitizeStringFilter::class],
            ], 'validators' => [
                ['name' => Digits::class]
            ]
        ];

        $filter['cost_gross'] = [
            'required'      => true,
            'filters'       => [
                ['name' => SanitizeStringFilter::class],
            ], 'validators' => [
                ['name' => FloatValidator::class]
            ]
        ];

        $filter['cost_date'] = [
            'required'      => true,
            'filters'       => [
                ['name' => SanitizeStringFilter::class],
            ], 'validators' => [
                ['name' => Date::class]
            ]
        ];

        $filter['cost_docno'] = [
            'required'      => true,
            'filters'       => [
                ['name' => SanitizeStringFilter::class],
            ], 'validators' => [
                [
                    'name'    => StringLength::class,
                    'options' => [
                        'encoding' => 'UTF-8',
                        'min'      => 1,
                        'max'      => 80,
                    ]
                ]
            ]
        ];

        return $filter;
    }
}
