<?php

namespace Bitkorn\Cashbook\Pdf;

use Bitkorn\Images\Tools\Svg\SvgAspectRatio;
use Bitkorn\Trinket\Service\I18n\NumberFormatService;

class PdfHeaderFooterSimple extends PdfClass
{
    protected string $pdfFilename = '';
    protected NumberFormatService $numberFormatService;

    protected string $filepathPdf = '';

    protected string $protectPasswd = '';
    protected string $fontFamilyDefault = 'helvetica';
    protected int $footerPositionFromBottom = -25;
    protected SvgAspectRatio $svgAspectRatio;

    protected int $positionTopmost = 6;
    protected int $contentStart = 26;

    public function getPdfFilename(): string
    {
        return $this->pdfFilename;
    }

    /**
     * @var array From config
     */
    protected array $colorArrayBrand = [];
    protected string $pathLogo;
    protected int $logoWidth = 0;
    protected int $logoHeight = 12; // fix

    public function setNumberFormatService(NumberFormatService $numberFormatService): void
    {
        $this->numberFormatService = $numberFormatService;
    }

    public function setProtectPasswd(string $protectPasswd): void
    {
        $this->protectPasswd = $protectPasswd;
    }

    public function setColorArrayBrand(array $colorArrayBrand): void
    {
        $this->colorArrayBrand = $colorArrayBrand;
    }

    public function setPathLogo(string $pathLogo): void
    {
        $this->pathLogo = $pathLogo;
    }

    /**
     * 1. compute images width with aspect ratio (keep height)
     * 2. set PDF protection password
     * 3. set PDF meta data
     * 4. set color and font
     */
    public function makeBasics()
    {
        $this->svgAspectRatio = new SvgAspectRatio();
        if (!$this->svgAspectRatio->loadSvg($this->storageLocationImg . $this->pathLogo)) {
            throw new \RuntimeException('Unable to load SVG file: ' . $this->storageLocationImg . $this->pathLogo);
        }
        $this->logoWidth = ($this->svgAspectRatio->getWidth() / $this->svgAspectRatio->getHeight()) * $this->logoHeight;

        $this->SetProtection(['modify'], null, $this->protectPasswd);
        // PDF basics
        $this->SetCreator($this->creator);
        $this->SetAuthor($this->author);
        $this->SetTitle($this->title);
        $this->SetSubject($this->subject);
        $this->SetMargins($this->marginLeft, $this->marginTop, $this->marginRight); // left, top, right
        $this->SetAutoPageBreak(true, $this->marginBottom + (-1 * $this->footerPositionFromBottom));

        $this->SetFillColorArray($this->colorArrayWhite);
        $this->SetTextColorArray($this->colorArrayBlack);
        $this->SetFont($this->fontFamilyDefault, '');
        $this->SetFontSize($this->fontSizeInitial);
    }

    public function makeDocument(): void
    {
        $this->makeBasics();
    }

    public function AddPage($orientation = '', $format = '', $keepmargins = false, $tocpage = false)
    {
        parent::AddPage($orientation, $format, $keepmargins, $tocpage);
        $this->SetY($this->contentStart);
    }

    public function Header()
    {
        $this->SetFontSize($this->fontSizeInitial);
        $this->ImageSVG($this->storageLocationImg . $this->pathLogo, ($this->getPageWidth() / 2) - ($this->logoWidth / 2), $this->positionTopmost, null, $this->logoHeight);
        $this->SetY($this->positionTopmost + $this->logoHeight + 1);
        $this->Cell($this->getContentWidth(), 1, '', 'T', 1, 'C', false);
        $this->contentStart = $this->positionTopmost + $this->logoHeight + 1;
    }

    public function Footer()
    {
        $this->SetFontSize(PdfClass::FONT_SIZE_XXS);
        $this->SetFillColorArray($this->colorArrayWhite);
        $this->SetTextColorArray($this->colorArrayBlack);
        $this->SetY($this->getPageHeight() - 6);
        $this->Cell($this->getContentWidth(), 0, 'Seite ' . $this->getAliasNumPage() . '/' . $this->getAliasNbPages(), 0, 0, 'C');
    }

    /**
     * @return string
     * After the call of this function, $this holds ONLY members from base class TCPDF (all other are lost)!
     */
    public function writeDocument(): string
    {
        $this->Output($p = $this->filepathPdf, 'F');
        /*
         * echt krank: laut debugger ist man ab hier immernoch in abgeleiteten Klasse.
         * Es existieren aber keine Klassenvariablen aus PdfClass, PdfHeaderFooterBrand, PdfCorrespondenceDefault oder PdfOrderConfirm.
         * Es existieren nur Klassenvariablen aus TCPDF
         */
        return $p;
    }
}
