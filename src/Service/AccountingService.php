<?php

namespace Bitkorn\Cashbook\Service;

use Bitkorn\Cashbook\Entity\CostEntity;
use Bitkorn\Cashbook\Entity\EarnEntity;

class AccountingService extends AbstractCashbookService
{
    public function insertEarn(EarnEntity $earnEntity): string
    {
        return $this->earnTable->insertEarn($earnEntity);
    }

    public function insertCost(CostEntity $costEntity): string
    {
        return $this->costTable->insertCost($costEntity);
    }

    /**
     * @param string $clientUuid
     * @param string $dateFrom
     * @param string $dateTo
     * @return array List of earns.
     */
    public function getEarnsBetweenDates(string $clientUuid, string $dateFrom, string $dateTo): array
    {
        return $this->earnTable->getEarnsBetweenDates($clientUuid, $dateFrom, $dateTo);
    }

    /**
     * @param string $clientUuid
     * @param string $dateFrom
     * @param string $dateTo
     * @return array List of costs.
     */
    public function getCostsBetweenDates(string $clientUuid, string $dateFrom, string $dateTo): array
    {
        return $this->costTable->getCostsBetweenDates($clientUuid, $dateFrom, $dateTo);
    }

    /**
     * @param string $clientUuid
     * @param string $dateFrom
     * @param string $dateTo
     * @return array Earn sums (net & gross).
     */
    public function getEarnSumsBetweenDates(string $clientUuid, string $dateFrom, string $dateTo): array
    {
        return $this->earnTable->getEarnSumsBetweenDates($clientUuid, $dateFrom, $dateTo);
    }

    /**
     * @param string $clientUuid
     * @param string $dateFrom
     * @param string $dateTo
     * @return array List of cost sums (net & gross GROUPED BY cost_type)
     */
    public function getCostSumsBetweenDates(string $clientUuid, string $dateFrom, string $dateTo): array
    {
        return $this->costTable->getCostSumsBetweenDates($clientUuid, $dateFrom, $dateTo);
    }
}
