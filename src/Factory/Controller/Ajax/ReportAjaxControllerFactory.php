<?php

namespace Bitkorn\Cashbook\Factory\Controller\Ajax;

use Bitkorn\Cashbook\Controller\Ajax\ReportAjaxController;
use Bitkorn\Cashbook\Service\AccountingService;
use Bitkorn\Cashbook\Service\PdfDocumentService;
use Bitkorn\User\Service\UserService;
use Interop\Container\ContainerInterface;
use Laminas\ServiceManager\Exception\ServiceNotCreatedException;
use Laminas\ServiceManager\Exception\ServiceNotFoundException;
use Laminas\ServiceManager\Factory\FactoryInterface;

class ReportAjaxControllerFactory implements FactoryInterface
{
    /**
     * Create an object
     *
     * @param ContainerInterface $container
     * @param string $requestedName
     * @param null|array $options
     * @return object
     * @throws ServiceNotFoundException if unable to resolve the service
     * @throws ServiceNotCreatedException if an exception is raised when creating a service
     */
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        $controller = new ReportAjaxController();
        $controller->setLogger($container->get('logger'));
        $controller->setUserService($container->get(UserService::class));
        $controller->setAccountingService($container->get(AccountingService::class));
        $controller->setPdfDocumentService($container->get(PdfDocumentService::class));
        return $controller;
    }
}
