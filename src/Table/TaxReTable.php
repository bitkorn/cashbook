<?php

namespace Bitkorn\Cashbook\Table;

use Bitkorn\Trinket\Table\AbstractLibTable;
use Laminas\Db\ResultSet\HydratingResultSet;
use Laminas\Db\Sql\Delete;
use Laminas\Db\Sql\Expression;
use Laminas\Db\Sql\Select;
use Laminas\Db\Sql\Update;
use Laminas\Db\Sql\Where;

class TaxReTable extends AbstractLibTable
{
    /** @var string */
    protected $table = 'tax_re';

    /**
     * @param string $taxReUuid
     * @param string $clientUuid
     * @return array
     */
    public function getTaxRe(string $taxReUuid, string $clientUuid): array
    {
        $select = $this->sql->select();
        try {
            $select->where(['tax_re_uuid' => $taxReUuid, 'client_uuid' => $clientUuid]);
            /** @var HydratingResultSet $result */
            $result = $this->selectWith($select);
            if ($result->valid() && $result->count() == 1) {
                return $result->toArray()[0];
            }
        } catch (\Exception $exception) {
            $this->log($exception, __CLASS__, __FUNCTION__);
        }
        return [];
    }
}
