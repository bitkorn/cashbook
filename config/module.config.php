<?php

namespace Bitkorn\Cashbook;

use Bitkorn\Cashbook\Controller\Ajax\ListsAjaxController;
use Bitkorn\Cashbook\Controller\Ajax\ReportAjaxController;
use Bitkorn\Cashbook\Controller\Rest\Accounting\CostRestController;
use Bitkorn\Cashbook\Controller\Rest\Accounting\EarnRestController;
use Bitkorn\Cashbook\Factory\Controller\Ajax\ListsAjaxControllerFactory;
use Bitkorn\Cashbook\Factory\Controller\Ajax\ReportAjaxControllerFactory;
use Bitkorn\Cashbook\Factory\Controller\Rest\Accounting\CostRestControllerFactory;
use Bitkorn\Cashbook\Factory\Controller\Rest\Accounting\EarnRestControllerFactory;
use Bitkorn\Cashbook\Factory\Form\CostFormFactory;
use Bitkorn\Cashbook\Factory\Form\EarnFormFactory;
use Bitkorn\Cashbook\Factory\Pdf\PdfHeaderFooterSimpleFactory;
use Bitkorn\Cashbook\Factory\Pdf\Report\PdfReportTaxPreFactory;
use Bitkorn\Cashbook\Factory\Service\AccountingServiceFactory;
use Bitkorn\Cashbook\Factory\Service\PdfDocumentServiceFactory;
use Bitkorn\Cashbook\Factory\Table\ClientTableFactory;
use Bitkorn\Cashbook\Factory\Table\CostTableFactory;
use Bitkorn\Cashbook\Factory\Table\EarnTableFactory;
use Bitkorn\Cashbook\Factory\Table\TaxPreTableFactory;
use Bitkorn\Cashbook\Factory\Table\TaxReTableFactory;
use Bitkorn\Cashbook\Form\CostForm;
use Bitkorn\Cashbook\Form\EarnForm;
use Bitkorn\Cashbook\Pdf\PdfHeaderFooterSimple;
use Bitkorn\Cashbook\Pdf\Report\PdfReportTaxPre;
use Bitkorn\Cashbook\Service\AccountingService;
use Bitkorn\Cashbook\Service\PdfDocumentService;
use Bitkorn\Cashbook\Table\ClientTable;
use Bitkorn\Cashbook\Table\CostTable;
use Bitkorn\Cashbook\Table\EarnTable;
use Bitkorn\Cashbook\Table\TaxPreTable;
use Bitkorn\Cashbook\Table\TaxReTable;
use Laminas\Router\Http\Literal;
use Laminas\Router\Http\Segment;

return [
    'router'          => [
        'routes' => [
            /*
             * AJAX - lists
             */
            'bitkorn_cashbook_ajax_lists_costtype'  => [
                'type'    => Literal::class,
                'options' => [
                    'route'    => '/cashbook-lists-costtype',
                    'defaults' => [
                        'controller' => ListsAjaxController::class,
                        'action'     => 'costType'
                    ],
                ],
            ],
            /*
             * AJAX - report
             */
            'bitkorn_cashbook_ajax_report_createreport'  => [
                'type'    => Literal::class,
                'options' => [
                    'route'    => '/cashbook-report-create',
                    'defaults' => [
                        'controller' => ReportAjaxController::class,
                        'action'     => 'createReport'
                    ],
                ],
            ],
            'bitkorn_cashbook_ajax_report_createreporttaxprepdf'  => [
                'type'    => Literal::class,
                'options' => [
                    'route'    => '/cashbook-report-create-taxpre-pdf',
                    'defaults' => [
                        'controller' => ReportAjaxController::class,
                        'action'     => 'createPdfReportTaxPre'
                    ],
                ],
            ],
            /*
             * REST - earn
             */
            'bitkorn_cashbook_rest_accounting_earn' => [
                'type'    => Segment::class,
                'options' => [
                    'route'       => '/cashbook-rest-accounting-earn[/:id]',
                    'constraints' => [
                        'id' => '[0-9A-Fa-f-]+',
                    ],
                    'defaults'    => [
                        'controller' => EarnRestController::class
                    ],
                ],
            ],
            /*
             * REST - cost
             */
            'bitkorn_cashbook_rest_accounting_cost' => [
                'type'    => Segment::class,
                'options' => [
                    'route'       => '/cashbook-rest-accounting-cost[/:id]',
                    'constraints' => [
                        'id' => '[0-9A-Fa-f-]+',
                    ],
                    'defaults'    => [
                        'controller' => CostRestController::class
                    ],
                ],
            ],
        ],
    ],
    'controllers'     => [
        'factories'  => [
            // AJAX - lists
            ListsAjaxController::class => ListsAjaxControllerFactory::class,
            ReportAjaxController::class => ReportAjaxControllerFactory::class,
            // REST - earn
            EarnRestController::class  => EarnRestControllerFactory::class,
            CostRestController::class  => CostRestControllerFactory::class,
        ],
        'invokables' => [],
    ],
    'service_manager' => [
        'factories'  => [
            // table
            ClientTable::class       => ClientTableFactory::class,
            CostTable::class         => CostTableFactory::class,
            EarnTable::class         => EarnTableFactory::class,
            TaxPreTable::class       => TaxPreTableFactory::class,
            TaxReTable::class        => TaxReTableFactory::class,
            // service
            AccountingService::class => AccountingServiceFactory::class,
            PdfDocumentService::class => PdfDocumentServiceFactory::class,
            // form
            EarnForm::class          => EarnFormFactory::class,
            CostForm::class          => CostFormFactory::class,
            // PDF
            PdfHeaderFooterSimple::class => PdfHeaderFooterSimpleFactory::class,
            PdfReportTaxPre::class => PdfReportTaxPreFactory::class,
        ],
        'invokables' => [],
    ],
    'view_helpers'    => [
        'factories'  => [],
        'invokables' => [],
        'aliases'    => [],
    ],
    'view_manager'    => [
        'template_map'        => [],
        'template_path_stack' => [],
        'strategies'          => [
            'ViewJsonStrategy',
        ],
    ],
    'bitkorn_cashbook' => [
        'protect_passwd' => 'testtext',
        'path_pdf' => __DIR__ . '/../../../../data/files/documents/pdf',
        'path_img' => __DIR__ . '/../../../../data/files/documents/img',
        'brand_rgb' => [0, 102, 102],
        'image_paths' => [ // within 'path_img'
            'logo' => '/logo.svg',
        ],
        'data_creator' => 'Torsten Bitkorn',
        'data_author' => 'Torsten Bitkorn',
    ]
];
