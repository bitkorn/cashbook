<?php

namespace Bitkorn\Cashbook\Controller\Ajax;

use Bitkorn\Cashbook\Service\AccountingService;
use Bitkorn\Trinket\View\Model\JsonModel;
use Bitkorn\User\Controller\AbstractUserController;

class ListsAjaxController extends AbstractUserController
{
    protected AccountingService $accountingService;

    public function setAccountingService(AccountingService $accountingService): void
    {
        $this->accountingService = $accountingService;
    }

	/**
	 * @return JsonModel
	 */
	public function costTypeAction(): JsonModel
    {
		$jsonModel = new JsonModel();
        $jsonModel->setArr($this->accountingService->getCostTypes());
        $jsonModel->setSuccess(1);
		return $jsonModel;
	}
}
